exports.options = {
    routePrefix: '/documentation',
    exposeRoute: true,
    swagger: {
        info: {
            title: 'Pinecode Builder API',
            description: 'Build your mobile app easily via API.',
            version: '1.0.0'
        },
        externalDocs: {
            url: 'https://pinecode-builder.com',
            description: 'Build your mobile app easily via API.'
        },
        host: 'localhost',
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json']
    }
}
