const boom = require('boom')

// Get Data Models
const Build = require('../models/Build')

// Get all builds
module.exports.getBuilds = async (req, reply) => {
    try {
        const builds = await Build.find()
        return builds
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Get single build by ID
module.exports.getSingleBuild = async (req, reply) => {
    const { id } = req.params
    try {
        const build = await Build.findById(id)
        return build
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Add a new build
module.exports.addBuild = async (req, reply) => {
    const { name, framework } = req.body
    const newBuildBody = {
        name,
        framework
    }
    try {
        const newBuild = new Build(newBuildBody)
        return newBuild.save()
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Update an existing build
module.exports.updateBuild = async (req, reply) => {
    const { id } = req.params
    const { name, framework } = req.body
    const newBuildBody = {
        name,
        framework
    }
    try {
        const updatedBuild = await Build.findByIdAndUpdate(id, newBuildBody, { new: true })
        return updatedBuild
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Delete a build
module.exports.deleteBuild = async (req, reply) => {
    const { id } = req.params
    try {
        const deleteBuild = await Build.findByIdAndRemove(id)
        return deleteBuild
    } catch (err) {
        throw boom.boomify(err)
    }
}
