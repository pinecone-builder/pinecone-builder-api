const boom = require('boom')

// Get Data Models
const Framework = require('../models/Framework')

// Get all frameworks
module.exports.getFrameworks = async (req, reply) => {
    try {
        const frameworks = await Framework.find()
        return frameworks
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Get single framework by ID
module.exports.getSingleFramework = async (req, reply) => {
    const { id } = req.params
    try {
        const framework = await Framework.findById(id)
        return framework
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Add a new framework
module.exports.addFramework = async (req, reply) => {
    const { name, npmUrl, commands } = req.body
    const id = name.replace(/[^\w\s]/gi, '').replace(/\s/g, '-').toLowerCase()
    const newFrameworkBody = {
        _id: id,
        name,
        npmUrl,
        commands
    }
    try {
        const newFramework = new Framework(newFrameworkBody)
        return newFramework.save()
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Update an existing framework
module.exports.updateFramework = async (req, reply) => {
    const { id } = req.params
    const { name, npmUrl, commands } = req.body
    const newFrameworkBody = {
        name,
        npmUrl,
        commands
    }
    try {
        const updatedFramework = await Framework.findByIdAndUpdate(id, newFrameworkBody, { new: true })
        return updatedFramework
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Delete a framework
module.exports.deleteFramework = async (req, reply) => {
    const { id } = req.params
    const body = {
        _id: id
    }
    try {
        const deleteFramework = await Framework.findByIdAndRemove(body)
        return deleteFramework
    } catch (err) {
        throw boom.boomify(err)
    }
}
