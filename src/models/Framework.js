const mongoose = require('mongoose')

const frameworkSchema = new mongoose.Schema({
    _id: {
        type: String,
        require: true
    },
    name: {
        type: String,
        required: true,
    },
    npmUrl: {
        type: String,
        required: true,
    },
    commands: {
        type: Array,
        required: true
    }
})

module.exports = mongoose.model('Framework', frameworkSchema)
