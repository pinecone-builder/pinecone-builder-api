const mongoose = require('mongoose')

const buildSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    framework: {
        type: String,
        required: true,
        enum: ['react-native', 'flutter']
    }
})

module.exports = mongoose.model('Build', buildSchema)
