// Import our routes
const frameworkRoutes = require('./routes/framework-routes')
const buildRoutes = require('./routes/build-routes')

const routes = [...frameworkRoutes, ...buildRoutes]

module.exports = routes
