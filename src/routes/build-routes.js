// Import our Controllers
const buildController = require('../controllers/build-controller')

const buildRoutes = [
    {
        method: 'GET',
        url: '/builds',
        handler: buildController.getBuilds
    },
    {
        method: 'GET',
        url: '/builds/:id',
        handler: buildController.getSingleBuild
    },
    {
        method: 'POST',
        url: '/builds',
        handler: buildController.addBuild,
        // schema: documentation.addBuildSchema
    },
    {
        method: 'PUT',
        url: '/builds/:id',
        handler: buildController.updateBuild
    },
    {
        method: 'DELETE',
        url: '/builds/:id',
        handler: buildController.deleteBuild
    }
]

module.exports = buildRoutes
