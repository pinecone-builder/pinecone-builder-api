// Import our Controllers
const frameworkController = require('../controllers/framework-controller')

const frameworkRoutes = [
    {
        method: 'GET',
        url: '/frameworks',
        handler: frameworkController.getFrameworks
    },
    {
        method: 'GET',
        url: '/frameworks/:id',
        handler: frameworkController.getSingleFramework
    },
    {
        method: 'POST',
        url: '/frameworks',
        handler: frameworkController.addFramework,
        // schema: documentation.addFrameworkSchema
    },
    {
        method: 'PUT',
        url: '/frameworks/:id',
        handler: frameworkController.updateFramework
    },
    {
        method: 'DELETE',
        url: '/frameworks/:id',
        handler: frameworkController.deleteFramework
    }
]

module.exports = frameworkRoutes
