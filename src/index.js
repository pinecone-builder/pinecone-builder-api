const mongoose = require('mongoose')
// Require the framework and instantiate it
const fastify = require('fastify')({
    logger: true
})

const { NODE_ENV } = process.env || 'development'
const { NODE_PORT } = process.env || 3000
const mongoDbHost = NODE_ENV === 'production' ? 'mongo' : 'locahost'

const swagger = require('./config/swagger')
fastify.register(require('fastify-swagger'), swagger.options)

// Connect to DB
mongoose.connect(`mongodb://${mongoDbHost}/pinecone`)
    .then(() => console.log('MongoDB connected…'))
    .catch((err) => console.log(err))

const routes = require('./routes')

routes.map((route) => fastify.route(route))

// Declare a route
fastify.get('/', async (request, reply) => ({ hello: 'world' }))

// Run the server!
const start = async () => {
    try {
        await fastify.listen(NODE_PORT, '0.0.0.0')
        fastify.swagger()
        fastify.log.info(`listening on ${NODE_PORT}`)
    } catch (err) {
        fastify.log.error(err)
        process.exit(1)
    }
}

start()
