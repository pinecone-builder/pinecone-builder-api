# <img src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/7393293/pinecode-builder-logo.jpg?width=64" height=50 /> Pinecone Builder API
[![pipeline status](https://gitlab.com/pinecone-builder/pinecone-builder-api/badges/master/pipeline.svg)](https://gitlab.com/pinecone-builder/pinecone-builder-api/-/commits/master) [![coverage report](https://gitlab.com/pinecone-builder/pinecone-builder-api/badges/master/coverage.svg)](https://gitlab.com/pinecone-builder/pinecone-builder-api/-/commits/master)
> Start to develop easily your **your mobile app** with your custom starter pack via API.

## Available parameters:
📱  **Frameworks**
🔗  **Templates**
🔌  **Libraries**
🔧  **Configurations**
💅  **Styles** 

## Available frameworks:
- ⚛ React Native
- Flutter
- Ionic


# Roadmap
- [x] React Native
- [ ] Flutter
- [ ] Ionic

# Configuration
## 🐋Docker
Docker is connected for this project with a **docker-compose.yml**. It allows to run this API easily on *your own server* or *your own machine*. This *docker-compose.yml* run a Node environment and a MongoDB Database to allow our API to work.
### Environment:
- Node
- MongoDB
### Services:
- API
- pinecode-builder-cli
- Web Preview
- Web App

## 🦊Gitlab CI/CD
This project is connected via Gitlab CI/CD to automatically deploy the API on an external server.
By default it is configured for a simple commit on *master*.

# Setup & Start

1. Install **node_modules** `yarn` or `npm install`
2. **Start** the API `yarn start` or `npm run start`
